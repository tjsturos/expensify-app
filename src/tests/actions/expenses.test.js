import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { 
    startAddExpense, addExpense, 
    editExpense, startEditExpense, 
    removeExpense, startRemoveExpense, 
    setExpenses, startSetExpenses,
} from '../../actions/expenses';
import expenses from '../fixtures/expenses';
import database from '../../firebase/firebase'

const uid = 'test-uid';
const defaultAuthState = { auth: { uid } };
const createMockStore = configureMockStore([thunk])

beforeEach((done) => {
    const expenseData = {};
    expenses.forEach( ( {id, amount, description, note, createdAt } ) => {
        expenseData[id] = {amount, description, note, createdAt};
    })
    database.ref(`users/${uid}/expenses`).set(expenseData).then( () => done() );
})

test('should setup remove expense action object', () => {
    const action = removeExpense({ id: '123abc'});
    expect(action).toEqual({
        type: 'REMOVE_EXPENSE',
        id: '123abc'
    })
})


test('should remove expense from firebase', (done) => {
    const store = createMockStore(defaultAuthState);
    store.dispatch(startRemoveExpense(expenses[1])).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: "REMOVE_EXPENSE",
            id: expenses[1].id
        })
        database.ref(`users/${uid}/expenses/${expenses[1].id}`).once('value').then((snapshot) => {
            expect(snapshot.val()).toBeFalsy();
            done()
        })
    })

})

test('should setup edit expense action object', () => {
    const action = editExpense('abc', { description: '4445', number: 5 });
    expect(action).toEqual({
        type: 'EDIT_EXPENSE',
        id: 'abc',
        updates: {
            description: '4445',
            number: 5
        }
    })
})

test('should edit expense on given expense in database', (done) => {
    const store = createMockStore(defaultAuthState);
    const id = expenses[1].id;
    const updates = {
        note: "update",
        amount: 4512
    }

    store.dispatch(startEditExpense(id, updates)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: "EDIT_EXPENSE",
            id,
            updates
        });
        return database.ref(`users/${uid}/expenses/${id}`).once('value').then((snapshot) => {
            const {
                description = '', 
                note = '', 
                amount = 0, 
                createdAt = 0
            } = expenses[1];
            const expense = { description, note, amount, createdAt };
            expect(snapshot.val()).toEqual({
                ...expense,
                ...updates
            })
            done()
        })
    })
})

test('should setup add expense action object with provided values', () => {
   
    const action = addExpense(expenses[2]);
    expect(action).toEqual({
        type: 'ADD_EXPENSE',
        expense: expenses[2]
    })
})

test('should add expense to database and store', (done) => {
    const store = createMockStore(defaultAuthState);
    const expenseData = {
        description: "mouse",
        amount: 3500,
        note: "this is much better",
        createdAt: 1235677565
    }
    store.dispatch(startAddExpense(expenseData)).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseData
            }
        });

        return database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value')
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseData);
        done();
    });
})

test('should add expense to database and store with default values', (done) => {
    const store = createMockStore(defaultAuthState);
    
    const expenseDefaults = {
        note: '',
        createdAt: 0,
        description: '',
        amount: 0
    }

    store.dispatch(startAddExpense({})).then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
            type: "ADD_EXPENSE",
            expense: {
                id: expect.any(String),
                ...expenseDefaults
            }
        });

        return database.ref(`users/${uid}/expenses/${actions[0].expense.id}`).once('value')
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseDefaults);
        done();
    });
})

test('should setup set expenses action object with data', () => {
    const action = setExpenses(expenses);
    expect(action).toEqual({
        type: "SET_EXPENSES",
        expenses
    });
})

test('should fetch the expenses from firebase', (done) => {
    const store = createMockStore(defaultAuthState);
    store.dispatch(startSetExpenses()).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'SET_EXPENSES',
            expenses
        })
        done()
    })

})

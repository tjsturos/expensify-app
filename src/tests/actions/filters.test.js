import { 
    setTextFilter, 
    sortByDate, 
    sortByAmount, 
    setStartDate, 
    setEndDate } from '../../actions/filters';
import moment from 'moment';

test('should setup text filter object with provided values', () => {
    const action = setTextFilter("Test");
    expect(action).toEqual({
        type: "SET_TEXT_FILTER",
        text: "Test"
    })
});

test('should setup text filter object with default values', () => {
    const action = setTextFilter();
    expect(action).toEqual({
        type: "SET_TEXT_FILTER",
        text: ""
    })
});

test('should setup sort by filter object to sort by date', () => {
    const action = sortByDate();
    expect(action).toEqual({
        type: "SORT_BY_DATE"
    })
})

test('should setup sort by filter object to sort by amount', () => {
    const action = sortByAmount();
    expect(action).toEqual({
        type: "SORT_BY_AMOUNT"
    })
})

test('should setup start date filter object to sort by amount', () => {
    const action = setStartDate(moment(0));
    expect(action).toEqual({
        type: "SET_START_DATE",
        startDate: moment(0)
    })
})

test('should setup end date filter object to sort by amount', () => {
    const action = setEndDate(moment(0));
    expect(action).toEqual({
        type: "SET_END_DATE",
        endDate: moment(0)
    })
})
import selectExpensesTotal from '../../selectors/expenses-total'
import expenses from '../fixtures/expenses'


test('should return 0 if there are no expenses', () => {
    const sum = selectExpensesTotal([]);
    expect(sum).toBe(0)
});

test('should return a sum of one expense (one expense showing)', () => {
    const amount = 145;
    const sum = selectExpensesTotal([{amount}]);
    expect(sum).toBe(amount)
})

test('should return a sum of multiple expenses', () => {
    const sum = selectExpensesTotal(expenses);

    let expectedSum = 0;

    expenses.forEach((expense) => {
        expectedSum += expense.amount
    });

    expect(sum).toBe( expectedSum);
})

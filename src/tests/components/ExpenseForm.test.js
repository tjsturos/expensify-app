import React from 'react';
import { shallow } from 'enzyme';
import ExpenseForm from '../../components/ExpenseForm';
import expenses from '../fixtures/expenses';
import moment from 'moment';

test('should render default expense form correctly', () => {
    const wrapper = shallow(<ExpenseForm />)
    expect(wrapper).toMatchSnapshot();
})

test('should render expense form with data correctly', () => {
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} />);
    expect(wrapper).toMatchSnapshot();
})

test('should render an error for invalid form submission', () => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();
    wrapper.find('form').simulate('submit', {
        preventDefault: () => { }
    })
    expect(wrapper.state('errorMessage').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
})

test('should change the desription input', () => {
    const name = 'description';
    const value = "new description";
    let wrapper = shallow(<ExpenseForm />);
    wrapper.find('input').at(0).simulate('change', {
        target: { name, value }
    });
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.state(name)).toBe(value);
})

test('should change the note input', () => {
    const name = 'note';
    const value = "some random note";
    let wrapper = shallow(<ExpenseForm />);
    wrapper.find('textarea').simulate('change', {
        target: { name, value }
    });
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.state(name)).toBe(value);
})

test('should change the amount if valid data', () => {
    const name = 'amount';
    const value = "521.11";
    let wrapper = shallow(<ExpenseForm />);
    wrapper.find('input').at(1).simulate('change', {
        target: { value }
    });
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.state(name)).toBe(value);
})

test('should NOT change the amount if invalid data', () => {
    const name = 'amount';
    const value = "12.222";
    let wrapper = shallow(<ExpenseForm />);
    wrapper.find('input').at(1).simulate('change', {
        target: { value }
    });
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.state(name)).toBe("");
})

test('Should call onSubmit prop for valid form submission',  () => {
    const onSubmitSpy = jest.fn();
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy}/>)
    wrapper.find('form').simulate('submit', {
        preventDefault: () => { }
    });
    expect(wrapper.state('errorMessage')).toBe(undefined);
    expect(onSubmitSpy).toHaveBeenLastCalledWith({
        description: expenses[0].description,
        amount: expenses[0].amount,
        note: expenses[0].note,
        createdAt: expenses[0].createdAt
    });
})

test('Should set new date on date change', () => {
    const now = moment();
    const wrapper = shallow(<ExpenseForm />);
    wrapper.find('SingleDatePicker').prop('onDateChange')(now);
    expect(wrapper.state('createdAt')).toEqual(now);
})

test('Should set calendar focus on change', () => {
    const calendarFocus = true;
    const wrapper = shallow(<ExpenseForm />);
    wrapper.find('SingleDatePicker').prop('onFocusChange')({focused: calendarFocus});
    expect(wrapper.state('calendarFocus')).toEqual(calendarFocus);
})
import React from 'react'
import { shallow } from 'enzyme'
import { ExpensesSummary } from '../../components/ExpensesSummary'
import expenses from '../fixtures/expenses'

test('should render ExpenseListItem correctly with no expenses', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={0} expensesTotal={0} />)
    expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseListItem correctly with 1 expense', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={1} expensesTotal={123} />)
    expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseListItem correctly with multiple expenses', () => {
    const wrapper = shallow(<ExpensesSummary expenseCount={2} expensesTotal={301} />)
    expect(wrapper).toMatchSnapshot()
})
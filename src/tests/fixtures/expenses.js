import moment from 'moment';
const expenses = [{
    id: '0',
    description: "Gum",
    note: '',
    amount: 195,
    createdAt: moment(0).valueOf()
}, {
    id: '1',
    description: "Rent",
    note: '',
    amount: 19500,
    createdAt: moment(0).subtract(4, 'days').valueOf()
}, {
    id: '2',
    description: "Bike", 
    note: 'dadsfa',
    amount: 2000,
    createdAt: moment(0).add(10, 'days').valueOf()
} ]

export default expenses;
import moment from 'moment';

const filters = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined
}

const altFilters = {
    text: 'bill',
    sortBy: 'date',
    startDate: moment(0),
    endDate: moment(198)
}

export { filters, altFilters };
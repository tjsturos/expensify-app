import React from 'react';
import { connect } from 'react-redux';
import ExpenseListItem from './ExpenseListItem';
import selectExpenses from '../selectors/expenses';

export const ExpenseList = (props) => (
    <div className="content-container">
        <div className="list-header">
            <div className="show-for-mobile">Expenses</div>
            <div className="show-for-desktop">Expense</div>
            <div className="show-for-desktop">Amount</div>
        </div>
        {
            props.expenses.length === 0 ? (
                <div className="list-item list-item--message">
                    <span>No expenses found.</span>
                </div>
            ) : (
                <div className="list-body">
                    {listExpenses(props.expenses)}
                </div>
            )
        }
    </div>
)



const listExpenses = (expenses) => {
    return expenses.map((expense) => {
        return <ExpenseListItem key={expense.id} {...expense} />;
    })
}


const mapStateToProps = (state) => {
    return {
        expenses: selectExpenses(state.expenses, state.filters)
    }
}

export default connect(mapStateToProps)(ExpenseList);

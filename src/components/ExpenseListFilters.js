import React from 'react';
import { DateRangePicker } from 'react-dates';
import { connect } from 'react-redux';
import { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate } from '../actions/filters';

export class ExpenseListFilters extends React.Component {

    state = {
        calendarFocused: null
    }

    onDatesChange = ({ startDate, endDate }) => {
        this.props.setStartDate(startDate);
        this.props.setEndDate(endDate);
    }

    onFocusChange = (calendarFocused) => {
        this.setState(() => ({ calendarFocused }));
    }

    onSelect = (e) => {
        e.target.value === 'date' ? 
        this.props.sortByDate() :
        this.props.sortByAmount();
    }

    onTextFilterChange = (e) => {
        const value = e.target.value;
        this.props.setTextFilter(value);
    }
    render() {
            return (
                <div className="content-container">
                    <div className="input-group">
                        <div className="input-group__item">
                            <input 
                                className="text-input"
                                placeholder="Search Expenses"
                                type='text' 
                                value={this.props.filters.text} 
                                onChange={this.onTextFilterChange}
                            />
                        </div>
                        <div className="input-group__item">
                            <select 
                                className="select"
                                value={this.props.filters.sortBy}
                                onChange={this.onSelect}>
                                    <option value="date">Date</option>
                                    <option value='amount'>Amount</option>
                            </select>
                        </div>
                        <div className="input-group__item">
                            <DateRangePicker
                                startDate={this.props.filters.startDate} // momentPropTypes.momentObj or null,
                                endDate={this.props.filters.endDate} // momentPropTypes.momentObj or null,
                                onDatesChange={this.onDatesChange} // PropTypes.func.isRequired,
                                focusedInput={this.state.calendarFocused} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={this.onFocusChange} // PropTypes.func.isRequired,
                                showClearDates={true}
                                numberOfMonths={1}
                                isOutsideRange={() =>  false}
                            />
                        </div>
                    </div>

                   
                    
                    
                </div>
            )
    }
};

const mapStateToProps = (state) => ({
        filters: state.filters
})

const mapDispatchToProps = (dispatch) => ({
    setTextFilter: (value) => dispatch(setTextFilter(value)),
    setStartDate: (startDate) => dispatch(setStartDate(startDate)),
    setEndDate: (endDate) => dispatch(setEndDate(endDate)),
    sortByAmount: () => dispatch(sortByAmount()),
    sortByDate: () => dispatch(sortByDate())
})

export default connect(mapStateToProps, mapDispatchToProps)(ExpenseListFilters);
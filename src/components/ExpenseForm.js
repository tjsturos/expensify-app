import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';


const now = moment().format('MMM Do, YYYY');
export default class ExpenseForm extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                amount : props.expense ? parseFloat(props.expense.amount / 100).toFixed(2) : '',
                description : props.expense ? props.expense.description : '',
                note: props.expense ? props.expense.note : '',
                createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
                calendarFocus : false,
                errorMessage : undefined
            }
        }

    onAmountChange(e) {
        const amount = e.target.value;
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }))
        }

        
    }

    onDateChange = (createdAt) => {
        if(createdAt) this.setState(() => ({ createdAt }));
        
    } 

    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calendarFocus: focused }));
    }

    onChange(e) {
        const input = e.target.name;
        const value = e.target.value;
        this.setState(() => ({ [input] : value}))
    }

    onSubmit = (e) => {
        e.preventDefault();

        if (!this.state.description || !this.state.amount) {
            !this.state.description ? 
            this.setState(() => ({ errorMessage : "Please provide a description of this expense!"})) :
            this.setState(() => ({ errorMessage : "Please provide an amount for this expense!"}));
        } else {
            this.setState(() => ({ errorMessage: undefined }));
            this.props.onSubmit({
                description: this.state.description,
                amount : parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            });
        }
        
    }
    render() {
        return (
            <form className="form" onSubmit={this.onSubmit}>
            
                <input 
                    className="text-input"
                    name="description" 
                    type="text"
                    placeholder="Description"
                    value={this.state.description}
                    onChange={this.onChange.bind(this)}
                    autoFocus
                />
            
                    
                <input 
                    className="text-input"
                    name="amount" 
                    type="text"
                    placeholder="Amount"
                    value={this.state.amount}
                    onChange={this.onAmountChange.bind(this)}
                />
                <SingleDatePicker
                    date={this.state.createdAt} // momentPropTypes.momentObj or null
                    onDateChange={this.onDateChange} // PropTypes.func.isRequired
                    focused={this.state.calendarFocus} // PropTypes.bool
                    onFocusChange={this.onFocusChange} // PropTypes.func.isRequired
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                />
                <textarea
                    className="text-area"
                    name="note"
                    value={this.state.note}
                    placeholder="Add a note for later. (optional)"
                    onChange={this.onChange.bind(this)}
                >
                </textarea>
                <div>
                    <button className="button button__default">{this.props.expense ? "Update Expense" : "Add Expense"}</button>
                </div>
                {this.state.errorMessage ? <div className="form__error">{this.state.errorMessage}</div> : undefined}
            </form>
        )

    }
   
}

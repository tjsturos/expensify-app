// Higher Order Component (HOC) - A component (HOC) that renders another component

import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The info is: {props.info}</p>
    </div>
)

const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin ? <p>This is private info. Please don't share!</p> : undefined}
            <WrappedComponent {...props}/>
        </div>
    )
};

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAuthenticated ? <WrappedComponent {...props}/> : <div>You are not logged in</div>}
        </div>
    )
};



const AdminInfo = withAdminWarning(Info)
const AuthInfo = requireAuthentication(AdminInfo);
ReactDOM.render(<AuthInfo isAuthenticated={true} isAdmin={true} info="There are the details..." />, document.getElementById('app'));
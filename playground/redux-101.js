import { createStore } from 'redux';

const countReducer = (state = { count: 0 }, action) => {
    
        switch (action.type) {
            case 'INCREMENT':
                return {
                    count: state.count + action.incrementBy
                };
            case 'DECREMENT':
                return {
                    count: state.count - action.decrementBy
                };
            case 'SET':
                return {
                    count: action.count
                };
            case 'RESET' :
                return {
                    count: 0
                };
            default:
                return state;
        }
       
    }

const store = createStore(countReducer);

const unsubscribe = store.subscribe(() => {
    console.log(store.getState());
})



const incrementCount = ({ incrementBy = 1 } = {}) => {
    const payload = { type: 'INCREMENT', incrementBy };
    store.dispatch(payload);
};
const decrementCount = ({ decrementBy = 1 } = {}) => {
    const payload = { type: 'DECREMENT', decrementBy };
    store.dispatch(payload);
};
const setCount = ({ count } = {}) => {
    const payload = { type: 'SET', count };
    store.dispatch(payload);
};
const resetCount = () => {
    const payload = { type: 'RESET' };
    store.dispatch(payload);
}

incrementCount({incrementBy: 5});
incrementCount();
decrementCount();
decrementCount({decrementBy: 2});
resetCount();
setCount({count: 55});
// Object destructuring

// const person = {
//     name: "Tyler",
//     age: 25,
//     location: {
//         city: "Minneapolis",
//         temp: 55
//     }
// };

// const { name = "Anonymous" , age} = person;

// console.log(`${name} is ${age}`);

// const { city, temp: temperature } = person.location;
// if (city && temperature) {
//     console.log(`It's ${temperature} in ${city}`);
// }

// const book = {
//     title: "Ego is the Enemy",
//     author: "Ryan Holiday",
//     publisher: {
//         name: 'Penguin'
//     }
// }

// const { name:publisherName = 'Self-Published'} = book.publisher;

// console.log(publisherName)

// Array Destructuring

// const address = ['1234 Chocolatte Blvd N', "Minneapolis", "Minnesota", "54321"];

// const [, city, state] = address;
// console.log(`You are in ${city}, ${state}.`)

const item = ['Coffee (hot)', '$2.00', '$2.60', '$2.90'];

const [drink, ,medium_price] = item;

console.log(`A medium ${drink} costs ${medium_price}.`)